import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterFormComponent } from './components/auth/register-form/register-form.component';
import { HeaderComponent } from './components/header/header.component';

const routes: Routes = [
  {path: 'header', component: HeaderComponent},
  {path:'login', component: LoginComponent},
  {path:'register', component: RegisterFormComponent},
  {path: '', redirectTo:'/header', pathMatch: 'full'},
  //{path: 'header', component: HeaderComponent},
  //para cuaquier ruta que no existe
  {path: '**', redirectTo: '/header'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
