import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

//recaptcha
//import { ReCaptchaService } from 'angular-recaptcha3';
//import { RecaptchaErrorParameters } from "ng-recaptcha";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  /* public captchaResponse = "";
  public resolved(captchaResponse: string): void {
    const newResponse = captchaResponse
      ? `${captchaResponse.substr(0, 7)}...${captchaResponse.substr(-7)}`
      : captchaResponse;
    this.captchaResponse += `${JSON.stringify(newResponse)}\n`;
  } */

  /* public onError(errorDetails: RecaptchaErrorParameters): void {
    this.captchaResponse += `ERROR; error details (if any) have been logged to console\n`;
    console.log(`reCAPTCHA error encountered; details:`, errorDetails);
  } */

  successMessage:string ="";
  loginForm!: FormGroup;
  constructor(
    private fb: FormBuilder,
    //recaptcha
    /* private recaptchaService: ReCaptchaService */) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email:['',[Validators.required, Validators.pattern("[A-Za-z0-9]*@gmail.com")]],
      password:['',[Validators.required,Validators.pattern("A-Za-z0-9@!_]{6,}")]]
    })
  }

  login(){
    this.successMessage="Successfully Loggined In..."
  }

  /* onCaptchaExpired(event: any) {
    console.log(event);
  }

  onCaptchaResponse(event: any) {
      console.log(event);
  } */

 /*  login() {
    this.recaptchaService.execute({action: ‘login’}).then(token => {
      // Backend verification method
      this.sendTokenToBackend(token);
    });
  } */

  /*flag:boolean = true

  apply(value:string){
    this.flag = value == "login"? true : false;
  }*/

}
