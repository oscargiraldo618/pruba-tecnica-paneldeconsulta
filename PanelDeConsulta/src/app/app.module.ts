import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './components/header/header.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterFormComponent } from './components/auth/register-form/register-form.component';

//recaptcha
//import { ReCaptchaModule } from 'angular-recaptcha3';
//import { RecaptchaModule } from 'ng-recaptcha';

//import { InvisibleDemoComponent } from 'invisible-demo.component';
//import { settings } from './invisible-demo.data';
//import { ReCaptchaModule } from 'angular-recaptcha3';
/**//* import {
  RecaptchaModule,
  RECAPTCHA_SETTINGS,
  RecaptchaSettings,
  RecaptchaFormsModule,
  RECAPTCHA_V3_SITE_KEY,
  RecaptchaV3Module
} from 'ng-recaptcha'; 

const RECAPTCHA_V3_STACKBLITZ_KEY = '6LeHBK0bAAAAAOQVTvBOWhfb08cQfUpFoSE3FsmP';
const RECAPTCHA_V2_DUMMY_KEY = '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'; */

import { RecaptchaModule } from 'ng-recaptcha';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    RegisterFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,

    RecaptchaModule

   /*  RecaptchaModule,
    RecaptchaFormsModule,
    RecaptchaV3Module */

    //ReCaptchaModule,
    /* ReCaptchaModule.forRoot({
      invisible: { 
        sitekey: '6LdkBuIgAAAAAHX9Bb7yRv7HNNUCAo3FRlnKFIU6', 
      }, 
      normal: { 
          sitekey: '6LdkBuIgAAAAAH6uOb0tKkMjVAA950vQVu55YVVR', 
      }, 
      /* idioma: 'en'
    }), */
  ],
  providers: [
    /* {
      provide: RECAPTCHA_V3_SITE_KEY,
      useValue: RECAPTCHA_V3_STACKBLITZ_KEY
    },
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: {
        siteKey: RECAPTCHA_V2_DUMMY_KEY
      } as RecaptchaSettings
    } */
  ],
  bootstrap: [AppComponent]
})



export class AppModule { }
