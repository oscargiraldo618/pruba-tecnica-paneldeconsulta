'use strict'


const Route = use('Route')

//ruta login
Route.post('/login', 'AuthController.login')

//ruta agregar usuario
Route.resource('users', 'UserController')
  .apiOnly()
  .validator(new Map([
    //validadores
    [['users.store'], ['StoreUser']]
  ]))
